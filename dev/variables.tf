# Input variable declarations. 
# https://www.terraform.io/docs/configuration/variables.html
#

variable "allowed_account_ids" {
  description = "Provision resources only in these accounts"
  type        = list(string)
  default     = ["208273824684"]
}

variable "app_name" {
  description = "Name of VPC"
  type        = string
  default     = "mpm"
}

variable "environment" {
  description = "App environment"
  type        = string
  default     = "prod"
}

variable "vpc_region" {
  description = "AWS Region"
  type        = string
  default     = "us-east-1"
}


variable "vpc_name" {
  description = "Name of VPC"
  type        = string
  default     = "{$var.app_name}"
}

variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
  }
}
variable "vpc_cidr" {
  description = "CIDR block for VPC"
  type        = string
  default     = "10.0.0.0/16"
}

variable "vpc_azs" {
  description = "Availability zones for VPC"
  type        = list
  default     = ["us-east-1a", "us-east-1b", "us-east-1c"]
}

variable "vpc_private_subnets" {
  description = "Private subnets for VPC"
  type        = list(string)
  default     = []
}

variable "vpc_public_subnets" {
  description = "Public subnets for VPC"
  type        = list(string)
  default     = []
}

variable "vpc_enable_nat_gateway" {
  description = "Enable NAT gateway for VPC"
  type    = bool
  default = true
}

variable "ec2_ami" {
  description = "Name of AMI"
  type        = string
  default     = "ami-04ffdefa427ef1da2" #ami-04781752c9b20ea41"    #"ami-0a0ddd875a1ea2c7f"  #Ubuntu Server 16.04 LTS (HVM), SSD Volume Type
}