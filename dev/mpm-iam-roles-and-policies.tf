#####################
# IAM assumable roles
######################
module "lambda_execution_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  role_name = "mpm_lambda_execution"

  create_role  = true
  role_description = "role descriptions"

  #List of ARNs of IAM policies to attach to IAM role
  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole",
    aws_iam_policy.athena_query_policy.arn,
    aws_iam_policy.user_preference_policy.arn,
    aws_iam_policy.send_email_policy.arn,
    aws_iam_policy.rds_connect_policy.arn,
    aws_iam_policy.sagemaker_invoke_policy.arn,
    aws_iam_policy.lambda_invoke_policy.arn,

  ]
#####################
# IAM policies
######################
  # ARNs of AWS entities who can assume these roles
  # trusted_role_arns = [
  #  "arn:aws:iam::835367859851:user/anton",
  # ]
  
  # AWS Services that can assume these roles
  trusted_role_services = [
    "lambda.amazonaws.com"
  ]
}



resource "aws_iam_policy" "send_email_policy" {
  name        = "mpm_send_email"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
#############################
resource "aws_iam_policy" "user_preference_policy" {
  name        = "mpm_user_preference"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "cognito-idp:AdminUpdateUserAttributes",
        "cognito-idp:AdminGetUser"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
##############################
resource "aws_iam_policy" "sagemaker_invoke_policy" {
  name        = "mpm_sagemaker_invoke"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "sagemaker:InvokeEndpoint"
        ], 
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

##############################
resource "aws_iam_policy" "rds_connect_policy" {
  name        = "mpm_rds_connect"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "rds-db:connect"
        ], 
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# #############################
resource "aws_iam_policy" "athena_query_policy" {
  name        = "mpm_athena_query"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "athena:StartQueryExecution",
        "athena:StopQueryExecution",
        "athena:GetQueryExecution",
        "athena:GetQueryResults",
        "s3:ListBucket",
        "glue:GetTable"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
# #############################
resource "aws_iam_policy"  "lambda_invoke_policy" {
  name        = "mpm_lambda_invoke"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "lambda:InvokeFunction"  
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}
#############################
resource "aws_iam_policy" "mpm_ci_policy" {
  name        = "mpm_ci"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
                "lambda:CreateFunction",
                "lambda:TagResource",
                "ec2:DescribeInstances",
                "lambda:GetFunctionConfiguration",
                "ec2:DescribeSnapshots",
                "cloudfront:CreateInvalidation",
                "ecr:DeleteRepository",
                "ec2:DescribeHostReservationOfferings",
                "ec2:DescribeVolumeStatus",
                "ec2:CreateNetworkInterfacePermission",
                "ec2:DescribeScheduledInstanceAvailability",
                "ec2:DescribeVolumes",
                "ec2:DescribeFpgaImageAttribute",
                "ec2:DescribeExportTasks",
                "lambda:ListLayers",
                "lambda:GetAlias",
                "ec2:DescribeKeyPairs",
                "ec2:DescribeReservedInstancesListings",
                "ec2:DescribeCapacityReservations",
                "lambda:ListFunctions",
                "ec2:DescribeSpotFleetRequestHistory",
                "lambda:GetEventSourceMapping",
                "ec2:DescribeVpcClassicLinkDnsSupport",
                "ec2:DescribeSnapshotAttribute",
                "lambda:ListAliases",
                "ec2:DescribeIdFormat",
                "ec2:ModifyNetworkInterfaceAttribute",
                "ecr:GetAuthorizationToken",
                "ec2:DescribeVolumeAttribute",
                "ec2:DescribeImportSnapshotTasks",
                "ec2:CreateNetworkInterface",
                "lambda:UpdateFunctionCode",
                "ec2:GetPasswordData",
                "ec2:DescribeVpcEndpointServicePermissions",
                "ec2:DescribeTransitGatewayAttachments",
                "ec2:DescribeScheduledInstances",
                "ec2:DescribeImageAttribute",
                "ec2:DescribeFleets",
                "lambda:PublishVersion",
                "ec2:DescribeReservedInstancesModifications",
                "ec2:DescribeSubnets",
                "ec2:DescribeMovingAddresses",
                "ec2:DescribeFleetHistory",
                "ec2:DescribePrincipalIdFormat",
                "ec2:DescribeFlowLogs",
                "ec2:DescribeRegions",
                "ec2:DescribeTransitGateways",
                "ec2:DescribeVpcEndpointServices",
                "ec2:DescribeSpotInstanceRequests",
                "ecr:ListImages",
                "lambda:UntagResource",
                "ec2:DescribeVpcAttribute",
                "lambda:ListTags",
                "ec2:DescribeTransitGatewayRouteTables",
                "ec2:DescribeAvailabilityZones",
                "ec2:DescribeNetworkInterfaceAttribute",
                "ec2:DescribeVpcEndpointConnections",
                "ec2:DescribeInstanceStatus",
                "lambda:UpdateEventSourceMapping",
                "ec2:DescribeHostReservations",
                "lambda:UpdateFunctionConfiguration",
                "ec2:DescribeBundleTasks",
                "ec2:DescribeIdentityIdFormat",
                "ec2:DescribeClassicLinkInstances",
                "ec2:DescribeVpcEndpointConnectionNotifications",
                "ec2:DescribeSecurityGroups",
                "ec2:DescribeFpgaImages",
                "ec2:DescribeVpcs",
                "ec2:DescribeStaleSecurityGroups",
                "ec2:DescribeAggregateIdFormat",
                "ec2:DescribeVolumesModifications",
                "ec2:GetHostReservationPurchasePreview",
                "ecr:BatchDeleteImage",
                "ec2:DescribeByoipCidrs",
                "ec2:GetConsoleScreenshot",
                "ec2:DescribePlacementGroups",
                "ec2:DescribeInternetGateways",
                "ec2:SearchTransitGatewayRoutes",
                "ec2:GetLaunchTemplateData",
                "ec2:DescribeSpotDatafeedSubscription",
                "ec2:DescribeAccountAttributes",
                "lambda:ListLayerVersions",
                "ec2:DescribeNetworkInterfacePermissions",
                "ec2:DescribeReservedInstances",
                "s3:HeadBucket",
                "ec2:DescribeNetworkAcls",
                "ec2:DescribeRouteTables",
                "ec2:DescribeEgressOnlyInternetGateways",
                "ec2:DescribeLaunchTemplates",
                "ecr:CreateRepository",
                "ec2:DescribeVpnConnections",
                "ec2:DescribeVpcPeeringConnections",
                "ecr:GetDownloadUrlForLayer",
                "ec2:ResetNetworkInterfaceAttribute",
                "ec2:DescribeReservedInstancesOfferings",
                "ec2:DeleteNetworkInterface",
                "ec2:GetTransitGatewayAttachmentPropagations",
                "ec2:DescribeFleetInstances",
                "ec2:DescribeVpcEndpointServiceConfigurations",
                "ec2:DescribePrefixLists",
                "ec2:GetReservedInstancesExchangeQuote",
                "ec2:DescribeInstanceCreditSpecifications",
                "ec2:DescribeVpcClassicLink",
                "ecr:PutImage",
                "lambda:UpdateAlias",
                "ec2:GetTransitGatewayRouteTablePropagations",
                "ecr:BatchGetImage",
                "ecr:DescribeImages",
                "lambda:ListEventSourceMappings",
                "ec2:DescribeVpcEndpoints",
                "ec2:DescribeElasticGpus",
                "ec2:DescribeVpnGateways",
                "lambda:CreateAlias",
                "ec2:DescribeAddresses",
                "lambda:ListVersionsByFunction",
                "lambda:GetLayerVersion",
                "ec2:DescribeInstanceAttribute",
                "lambda:PublishLayerVersion",
                "ec2:DescribeDhcpOptions",
                "lambda:GetAccountSettings",
                "lambda:CreateEventSourceMapping",
                "lambda:GetLayerVersionPolicy",
                "lambda:PutFunctionConcurrency",
                "ec2:GetConsoleOutput",
                "ec2:DescribeSpotPriceHistory",
                "ec2:DeleteNetworkInterfacePermission",
                "ec2:DescribeNetworkInterfaces",
                "ecr:DescribeRepositories",
                "ec2:GetTransitGatewayRouteTableAssociations",
                "ec2:DetachNetworkInterface",
                "ec2:DescribeIamInstanceProfileAssociations",
                "lambda:GetFunction",
                "ec2:DescribeTags",
                "ec2:DescribeLaunchTemplateVersions",
                "ec2:DescribeImportImageTasks",
                "ec2:DescribeNatGateways",
                "ec2:DescribeCustomerGateways",
                "ec2:DescribeSpotFleetRequests",
                "ec2:DescribeHosts",
                "ec2:DescribeImages",
                "ec2:DescribeSpotFleetInstances",
                "ec2:DescribeSecurityGroupReferences",
                "ec2:DescribePublicIpv4Pools",
                "ec2:AttachNetworkInterface",
                "ec2:DescribeTransitGatewayVpcAttachments",
                "lambda:GetPolicy",
                "ec2:DescribeConversionTasks"
      ],
      "Effect": "Allow",
      "Resource": "*"
    },
    {
      "Sid": "VisualEditor1",
      "Effect": "Allow",
      "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "iam:GetRole",
                "iam:PassRole",
                "iam:ListRoleTags",
                "s3:ListBucket",
                "s3:DeleteObject",
                "iam:ListRolePolicies",
                "iam:GetRolePolicy"
            ],
      "Resource": [
        "*"
        ]
    }
  ]
}
EOF
}