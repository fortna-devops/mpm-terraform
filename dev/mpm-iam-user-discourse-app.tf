# IAM user with policies
# -----------------------
resource "aws_iam_user_policy" "discourse" {
  name = "MpmIamForDiscourseApp"
  user = aws_iam_user.discourse_user.name

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:List*",
        "s3:Get*",
        "s3:AbortMultipartUpload",
        "s3:DeleteObject",
        "s3:PutObject",
        "s3:PutObjectAcl",
        "s3:PutObjectVersionAcl",
        "s3:PutLifecycleConfiguration",
        "s3:CreateBucket",
        "s3:PutBucketCORS"
      ],
      "Effect": "Allow",
      "Resource": ["*"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:ListAllMyBuckets",
        "s3:HeadBucket"
      ],
      "Resource": "*"
    }

  ]
}
EOF
}

resource "aws_iam_user" "discourse_user" {
  name = "mpm-discourse-app-s3-access"
  path = "/system/"

  tags = {
    tag-key = "tag-value"
  }
}

resource "aws_iam_access_key" "discourse_key" {
  user = aws_iam_user.discourse_user.name
}
