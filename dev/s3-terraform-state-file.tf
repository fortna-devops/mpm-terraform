
# #Terraform statefile. Must not delete or move
# terraform {
#   backend "s3" {
#     bucket = "mpm-dev2-terraform"   # !!!!! MUST match bucket name created above
#     key = "dev"                     # !!!!! MUST match env name used above
#     region = "us-east-2"
#     #  ...
#   }
# }


#S3 bucket to hold terraform related stuff
module "s3-bucket-terraform" {
  source = "./modules/mpm-bucket"
  bucket_name = "${var.app_name}-${var.environment}-terraform"  #gives "mpm-<env_name>-terraform" which is used in terraform block
  vpc_region = var.vpc_region
  is_website_bucket = true
  vpc_tags = var.vpc_tags
}

resource "aws_s3_bucket" "s3_bucket_terraform" {
    bucket = "${var.app_name}-${var.environment}-terraform"
    acl    = "private"

    # TODO - replicate to the master account bc the state file is so important
    #replication_configuration = 

    #TODO
    #server_side_encryption_configuration = true

    tags = var.vpc_tags
    versioning {
        enabled = true
    }
}


resource "aws_s3_bucket_public_access_block" "bucket_pub_access_block" {
  bucket = aws_s3_bucket.s3_bucket_terraform.id

  block_public_acls   = true
  block_public_policy = true
}

resource "aws_iam_policy" "terraform_bucket_policy" {
  name        = "mpm_terraform_s3_bucket"
  path        = "/"
  description = "My test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:ListBucket",
        "s3:GetObject",
        "s3:PutObject"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# resource "aws_iam_policy" "terraform_bucket_policy" {
#   name        = "mpm_terraform_s3_bucket"
#   path        = "/"
#   description = "Terraform S3 bucket access policy"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Effect": "Allow",
#       "Action": "s3:ListBucket",
#       "Resource": "${aws_s3_bucket.s3_bucket_terraform.arn}"
#     },
#     {
#       "Effect": "Allow",
#       "Action": ["s3:GetObject", "s3:PutObject"],
#       "Resource": "*"    
#   ]
# }
# EOF
# }