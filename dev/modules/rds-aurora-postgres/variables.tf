
variable app_name {
    description = "Ids of sunets to which db belongs"
    type = string
    default = "app_name"
}
variable vpc_id  {
    description = "VPC id"
    type = string
    default = ""
}

variable private_subnet_ids {
    description = "Ids of sunets to which db belongs"
    type = list(string)
    default = []
}

variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
  }
}