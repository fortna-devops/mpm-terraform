#provider "aws" {
#  region = "us-east-1"
#}

######################################
# Data sources to get VPC and subnets
######################################
data "aws_vpc" "default" {
  #default = false
  id = var.vpc_id
}

data "aws_subnet_ids" "all" {
  vpc_id = var.vpc_id   #data.aws_vpc.default.id
}

#############
# RDS Aurora
#############
module "aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "2.20.0"
  # insert the 6 required variables here

  name                            = "${var.app_name}-prod" #"aurora-example-postgresql"
  engine                          = "aurora-postgresql"
  engine_version                  = "11.6"
  subnets                         = var.private_subnet_ids  #data.aws_subnet_ids.all.ids
  vpc_id                          = data.aws_vpc.default.id  
  replica_count                   = 1
  instance_type                   = "db.r5.large"

  #Specifies whether the underlying storage layer should be encrypted
  storage_encrypted               = false

  # Specifies whether IAM Database authentication should be enabled or not. 
  # Not all versions and instances are supported. Refer to the AWS documentation to see which versions are supported
  # iam_database_authentication_enabled  = false

  apply_immediately               = true
  skip_final_snapshot             = true
  db_parameter_group_name         = aws_db_parameter_group.aurora_db_postgres11_parameter_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_cluster_postgres11_parameter_group.id
  //  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]

  create_security_group = false
  #A list of Security Group ID's to allow access to.
  allowed_security_groups = [aws_security_group.aurora-ps-sg.id]
  security_group_description = ""

  # Name for an automatically created database on cluster creation
  # database_name = ""

  # DB snapshot to create this database from
  # snapshot_identifier = ""

  deletion_protection = true
}

resource "aws_db_parameter_group" "aurora_db_postgres11_parameter_group" {
  name        = "test-aurora-db-postgres11-parameter-group"
  family      = "aurora-postgresql11"
  description = "test-aurora-db-postgres11-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_postgres11_parameter_group" {
  name        = "test-aurora-postgres11-cluster-parameter-group"
  family      = "aurora-postgresql11"
  description = "test-aurora-postgres11-cluster-parameter-group"
}

############################
# Example of security group
############################
# resource "aws_security_group" "app_servers" {
#   name_prefix = "${var.app_name}-prod-"     #"app-servers-"
#   description = "For application servers"
#   vpc_id      =  var.vpc_id  #data.aws_vpc.default.id
# }

# resource "aws_security_group_rule" "allow_access" {
#   type                     = "ingress"
#   from_port                = module.aurora.this_rds_cluster_port
#   to_port                  = module.aurora.this_rds_cluster_port
#   protocol                 = "tcp"
#   source_security_group_id = aws_security_group.app_servers.id
#   security_group_id        = module.aurora.this_security_group_id
# }
