variable "bucket_name" {
  description = "Name of S3 bucket"
  type        = string
  default     = "test-bucket"
}

variable "vpc_region" {
  description = "Name of S3 bucket"
  type        = string
  default     = "us-east-1"
}

variable "is_website_bucket" {
  description = "Name of S3 bucket"
  type        = bool
  default     = false
}

variable "vpc_tags" {
  description = "Tags to apply to resources created by VPC module"
  type        = map(string)
  default     = {
    Terraform   = "true"
    Environment = "prod"
  }
}