# Terraform configuration
# For modules, see https://registry.terraform.io/modules/terraform-aws-modules

provider "aws" {
  region = var.vpc_region
  allowed_account_ids = var.allowed_account_ids
}


module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "v2.21.0"

  name = "${var.app_name}-${var.environment}"
  cidr = var.vpc_cidr

  azs             = var.vpc_azs
  private_subnets = var.vpc_private_subnets
  public_subnets  = var.vpc_public_subnets

  enable_nat_gateway = var.vpc_enable_nat_gateway
  single_nat_gateway = true   #Should be true if want a single shared NAT Gateway across all  private networks


  enable_dns_hostnames = true
  enable_dns_support   = true

  # VPC endpoint for API gateway
  enable_apigw_endpoint              = true

  # security groups to associate with the network interface for API GW  endpoint
  apigw_endpoint_security_group_ids  = [module.http80_sg.this_security_group_id,
                                        module.http443_sg.this_security_group_id]
  apigw_endpoint_private_dns_enabled = true

  # The ID of one or more subnets in which to create a network interface for API GW endpoint. 
  # Only a single subnet within an AZ is supported. If omitted, private subnets will be used.
  #apigw_endpoint_subnet_ids = []

  #true if you want to provision an S3 endpoint to the VPC
  # enable_s3_endpoint = true      

  tags = var.vpc_tags
}



