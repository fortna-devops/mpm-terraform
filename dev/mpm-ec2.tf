#############
# EC2
#############
module "ec2_instances" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "v2.12.0"

  name           = "Bastion host"
  instance_count = 1

  ami                    = var.ec2_ami
  instance_type          = "t2.micro"
  vpc_security_group_ids = [module.vpc.default_security_group_id,
                            aws_security_group.allow_tls.id]
  subnet_id              = module.vpc.public_subnets[0]


  #tags = var.vpc_tags
  

  # Need to do some initial setup on your instances, then provisioners 
  # let you upload files, run shell scripts, or install and trigger other 
  # software like configuration management tools, etc.
  #
  #  provisioner "local-exec" {
  #    command = "echo ${aws_instance.example.public_ip} > ip_address.txt"
  #  }
}
