### User
module "analytic_runner_user" {
  source = "terraform-aws-modules/iam/aws//modules/iam-user"

  name = "mpm-analytic-runner"

  #permissions_boundary = "arn:aws:iam::aws:policy/AlexaForBusinessFullAccess"

  create_iam_user_login_profile = false
  create_iam_access_key         = false
}

# ###Policy
# resource "aws_iam_policy" "lambda_invoke_policy" {
#   name        = "MpmLambdaAllowInvoke"
#   path        = "/"
#   description = "My test policy"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "lambda:InvokeFunction"
#       ],
#       "Effect": "Allow",
#       "Resource": "*"
#     }
#   ]
# }
# EOF
# }

#Attach a Managed Policy to a user
resource "aws_iam_user_policy_attachment" "AnalyticRunnerLambdaExecution-attach" {
  user       = module.analytic_runner_user.this_iam_user_name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_user_policy_attachment" "AnalyticRunnerLambdaRole-attach" {
  user       = module.analytic_runner_user.this_iam_user_name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaRole"
}
#################################

data "aws_iam_policy_document" "custom_ignition_policy" {
  statement {
    sid       = "LambdaInvokeFunctionAndRDSConnect"
    actions   = [
        "lambda:InvokeFunction",
        "rds-db:connect"
        ]
    resources = ["*"]
  }
}

module "analytic_runner_policy_attachment" {
  source = "terraform-aws-modules/iam/aws//modules/iam-policy"

  name        = "mpm_ignition_cc_access"
  path        = "/"
  description = "Analytic-Runner "

  policy = data.aws_iam_policy_document.custom_ignition_policy.json
}