### User
module "ignition_cc_user" {
  source = "terraform-aws-modules/iam/aws//modules/iam-user"

  name = "mpm-ignition-cc"

  #permissions_boundary = "arn:aws:iam::aws:policy/AlexaForBusinessFullAccess"

  create_iam_user_login_profile = false
  create_iam_access_key         = false
}

# ###Policy
# resource "aws_iam_policy" "lambda_invoke_policy" {
#   name        = "MpmLambdaAllowInvoke"
#   path        = "/"
#   description = "My test policy"

#   policy = <<EOF
# {
#   "Version": "2012-10-17",
#   "Statement": [
#     {
#       "Action": [
#         "lambda:InvokeFunction"
#       ],
#       "Effect": "Allow",
#       "Resource": "*"
#     }
#   ]
# }
# EOF
# }

#Attach a Managed Policy to a user
resource "aws_iam_user_policy_attachment" "ignitionKinesisFull-attach" {
  user       = module.ignition_cc_user.this_iam_user_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisFullAccess"
}

resource "aws_iam_user_policy_attachment" "ignitionKinesisAnalyticsFull-attach" {
  user       = module.ignition_cc_user.this_iam_user_name
  policy_arn = "arn:aws:iam::aws:policy/AmazonKinesisAnalyticsFullAccess"
}

#################################

data "aws_iam_policy_document" "custom_ignition_access_policy" {
  statement {
    sid       = "LambdaInvokeFunctionAndRDSConnect"
    actions   = [
        "glue:GetCrawler",
                "glue:GetTableVersions",
                "glue:GetPartitions",
                "rds:DescribeGlobalClusters",
                "rds:CreateDBSubnetGroup",
                "glue:UpdateCrawler",
                "glue:GetDevEndpoint",
                "glue:UpdateTrigger",
                "glue:GetTrigger",
                "rds:DownloadDBLogFilePortion",
                "s3:PutLifecycleConfiguration",
                "rds:DescribeSourceRegions",
                "glue:GetJobRun",
                "s3:PutObjectTagging",
                "s3:DeleteObject",
                "s3:GetBucketWebsite",
                "glue:GetJobs",
                "glue:GetTriggers",
                "s3:PutReplicationConfiguration",
                "s3:DeleteObjectVersionTagging",
                "s3:GetBucketNotification",
                "s3:GetReplicationConfiguration",
                "rds:DescribeReservedDBInstances",
                "s3:PutObject",
                "s3:PutBucketNotification",
                "glue:GetMapping",
                "glue:GetPartition",
                "rds:CreateDBSnapshot",
                "glue:StartCrawlerSchedule",
                "rds:CreateEventSubscription",
                "s3:GetLifecycleConfiguration",
                "s3:GetInventoryConfiguration",
                "s3:GetBucketTagging",
                "glue:GetClassifiers",
                "glue:CreateTrigger",
                "s3:ReplicateTags",
                "glue:StopTrigger",
                "glue:CreateUserDefinedFunction",
                "glue:StopCrawler",
                "s3:ListBucket",
                "rds:DescribeDBClusterBacktracks",
                "athena:*",
                "rds:DescribeReservedDBInstancesOfferings",
                "glue:GetCatalogImportStatus",
                "glue:CreateJob",
                "s3:AbortMultipartUpload",
                "glue:GetConnection",
                "s3:PutBucketTagging",
                "rds:CreateDBInstance",
                "glue:ResetJobBookmark",
                "rds:DescribeDBInstances",
                "rds:DescribeEngineDefaultClusterParameters",
                "glue:CreatePartition",
                "rds:DescribeEventCategories",
                "s3:PutBucketVersioning",
                "glue:UpdatePartition",
                "rds:DescribeEvents",
                "rds:AddTagsToResource",
                "s3:ListBucketMultipartUploads",
                "s3:PutMetricsConfiguration",
                "s3:PutObjectVersionTagging",
                "glue:BatchGetPartition",
                "s3:GetBucketVersioning",
                "rds:DescribeDBSnapshotAttributes",
                "s3:PutInventoryConfiguration",
                "glue:GetTable",
                "glue:GetDatabase",
                "s3:GetAccountPublicAccessBlock",
                "glue:GetDataflowGraph",
                "rds:ListTagsForResource",
                "s3:PutBucketWebsite",
                "s3:ListAllMyBuckets",
                "s3:PutBucketRequestPayment",
                "rds:CreateDBCluster",
                "glue:CreateDatabase",
                "s3:GetBucketCORS",
                "glue:GetPlan",
                "s3:GetObjectVersion",
                "glue:GetJobRuns",
                "glue:BatchCreatePartition",
                "rds:DescribeDBInstanceAutomatedBackups",
                "s3:PutAnalyticsConfiguration",
                "glue:GetDataCatalogEncryptionSettings",
                "glue:CreateClassifier",
                "s3:GetObjectVersionTagging",
                "glue:UpdateTable",
                "s3:CreateBucket",
                "rds:DescribeEngineDefaultParameters",
                "rds:CreateOptionGroup",
                "s3:ReplicateObject",
                "glue:GetSecurityConfiguration",
                "glue:GetResourcePolicy",
                "s3:GetObjectAcl",
                "glue:CreateScript",
                "glue:GetUserDefinedFunction",
                "s3:GetObjectVersionAcl",
                "s3:HeadBucket",
                "glue:StopCrawlerSchedule",
                "glue:GetUserDefinedFunctions",
                "glue:PutResourcePolicy",
                "glue:GetClassifier",
                "s3:DeleteObjectTagging",
                "s3:GetBucketPolicyStatus",
                "rds:CreateDBParameterGroup",
                "glue:UpdateDatabase",
                "glue:GetTables",
                "glue:CreateTable",
                "rds:DescribeDBSnapshots",
                "rds:DescribeDBSecurityGroups",
                "s3:PutBucketCORS",
                "glue:CreateConnection",
                "s3:ListMultipartUploadParts",
                "glue:CreateCrawler",
                "s3:GetObject",
                "glue:GetDevEndpoints",
                "rds:DescribeValidDBInstanceModifications",
                "s3:PutBucketLogging",
                "rds:DescribeOrderableDBInstanceOptions",
                "glue:StartJobRun",
                "s3:GetAnalyticsConfiguration",
                "rds:CreateDBClusterSnapshot",
                "rds:DescribeCertificates",
                "rds:CreateDBClusterParameterGroup",
                "s3:GetObjectVersionForReplication",
                "glue:UpdateClassifier",
                "rds:RemoveTagsFromResource",
                "rds:DescribeOptionGroups",
                "glue:GetJob",
                "s3:ListBucketByTags",
                "glue:GetConnections",
                "glue:GetCrawlers",
                "s3:PutAccelerateConfiguration",
                "rds:DescribeDBEngineVersions",
                "rds:DescribeDBSubnetGroups",
                "s3:DeleteObjectVersion",
                "s3:GetBucketLogging",
                "s3:ListBucketVersions",
                "s3:RestoreObject",
                "rds:DescribePendingMaintenanceActions",
                "rds:DescribeDBParameterGroups",
                "s3:GetAccelerateConfiguration",
                "s3:GetBucketPolicy",
                "glue:StartTrigger",
                "s3:PutEncryptionConfiguration",
                "s3:GetEncryptionConfiguration",
                "s3:GetObjectVersionTorrent",
                "glue:ImportCatalogToGlue",
                "glue:PutDataCatalogEncryptionSettings",
                "s3:GetBucketRequestPayment",
                "glue:StartCrawler",
                "s3:GetObjectTagging",
                "rds:DescribeDBParameters",
                "s3:GetMetricsConfiguration",
                "glue:UpdateJob",
                "rds:DescribeDBClusterSnapshotAttributes",
                "rds:DescribeDBClusterParameters",
                "s3:GetBucketPublicAccessBlock",
                "rds:DescribeEventSubscriptions",
                "glue:UpdateUserDefinedFunction",
                "rds:DescribeDBLogFiles",
                "glue:GetSecurityConfigurations",
                "s3:GetBucketAcl",
                "glue:GetDatabases",
                "s3:GetObjectTorrent",
                "glue:UpdateConnection",
                #"rds:CreateDBSecurityGroup",
                "rds:DescribeDBClusterSnapshots",
                "rds:DescribeOptionGroupOptions",
                "glue:CreateDevEndpoint",
                "glue:UpdateDevEndpoint",
                "rds:DownloadCompleteDBLogFile",
                "rds:DescribeDBClusterEndpoints",
                "rds:CreateDBInstanceReadReplica",
                "s3:GetBucketLocation",
                "glue:GetCrawlerMetrics",
                "rds:DescribeAccountAttributes",
                "rds:DescribeDBClusters",
                "s3:ReplicateDelete",
                "rds:DescribeDBClusterParameterGroups"
        ]
    resources = ["*"]
  }
}

module "ignition_cc_policy_attachment" {
  source = "terraform-aws-modules/iam/aws//modules/iam-policy"

  name        = "mpm_analytic_runner"
  path        = "/"
  description = "Analytic-Runner "

  policy = data.aws_iam_policy_document.custom_ignition_access_policy.json
}