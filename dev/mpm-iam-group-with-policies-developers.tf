############
# IAM users
############
# module "iam_mpm_developer1" {
#   source = "terraform-aws-modules/iam/aws//modules/iam-user"

#   name = "rtuok-dev2-admin"

#   create_iam_user_login_profile = false
#   create_iam_access_key         = false
# }
####################################################################################
# IAM group for superadmins with full Administrator access
#####################################################################################
# module "iam_group_superadmins" {
#   source = "terraform-aws-modules/iam/aws//modules/iam-group-with-policies"

#   name = "SuperAdmins"

#   group_users = [
#     module.iam_mpm_developer1.this_iam_user_name
#   ]

#   custom_group_policy_arns = [
#     "arn:aws:iam::aws:policy/AdministratorAccess",
#   ]
# }

#####################################################################################
# IAM group for users with custom access
#####################################################################################
module "iam_group_with_custom_Dev_policies" {
  source = "terraform-aws-modules/iam/aws//modules/iam-group-with-policies"

  name = "MpmDevelopers"
  attach_iam_self_management_policy = false

#   group_users = [
#     module.iam_mpm_ci.this_iam_user_name,
#     module.iam_user2.this_iam_user_name,
#   ]

  custom_group_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonCognitoReadOnly",
    #"arn:aws:iam::aws:policy/AlexaForBusinessFullAccess",
  ]

  custom_group_policies = [
    {
      name   = "mpm_s3_for_evelopers"
      policy = data.aws_iam_policy_document.developerS3Actions.json
    },
  ]
}

######################
# IAM policy (sample)
######################
data "aws_iam_policy_document" "developerS3Actions" {
  statement {
    sid       = "MpmS3ForDevelopers"
    actions = [
      "s3:ListBuckets",
      "s3:HeadBucket",
      "s3:PutObject",
      "s3:GetObject",
      "s3:DeleteObject",
    ]
    resources = ["*"]
  }
}