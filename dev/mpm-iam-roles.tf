#####################
# IAM assumable roles
######################

module "api_to_cloudwatch_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  role_name = "mpm_apigw_to_cloudwatch"

  create_role  = true
  role_description = "role descriptions"

  #List of ARNs of IAM policies to attach to IAM role
  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/service-role/AmazonAPIGatewayPushToCloudWatchLogs",
  ]

  # AWS Services that can assume these roles
  trusted_role_services = [
    "lambda.amazonaws.com"
  ]
}

# module "auth_apigw_role" {
#   source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
#   role_name = "${var.app_name}-auth-apigw

#   create_role  = true
#   role_description = "role descriptions"

#   #List of ARNs of IAM policies to attach to IAM role
#   custom_role_policy_arns = [
#     "????",
#   ]

#   # ARNs of AWS entities who can assume these roles
#   #trusted_role_arns = [
#   #  "arn:aws:iam::307990089504:root",
#   #  "arn:aws:iam::835367859851:user/anton",
#   #]
  
#   # AWS Services that can assume these roles
#   trusted_role_services = [
#     "lambda.amazonaws.com",
#     "edgelambda.amazonaws.com",
#   ]
# }

module "greengrass_access_role" {
  source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
  role_name = "mpm_greengrass_access"

  create_role  = true
  role_description = "role descriptions"

  #List of ARNs of IAM policies to attach to IAM role
  custom_role_policy_arns = [
    "arn:aws:iam::aws:policy/AmazonS3FullAccess",
    "arn:aws:iam::aws:policy/AmazonDynamoDBFullAccess",
    "arn:aws:iam::aws:policy/service-role/AWSGreengrassResourceAccessRolePolicy",
    "arn:aws:iam::aws:policy/AWSGreengrassFullAccess",
  ]

  # ARNs of AWS entities who can assume these roles
  #trusted_role_arns = [
  #  "arn:aws:iam::835367859851:user/anton",
  #]
  
  # AWS Services that can assume these roles
  trusted_role_services = [
    "greengrass.amazonaws.com",
  ]
}

# module "apigw_service_role" {
#   source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
#   role_name = "MpmApiGatewayService"

#   create_role  = true
#   role_description = "role descriptions"

#   #List of ARNs of IAM policies to attach to IAM role
#   custom_role_policy_arns = [                                   #Error! Cannot attach a Service Role Policy to a Customer Role.
#     "arn:aws:iam::aws:policy/aws-service-role/APIGatewayServiceRolePolicy",
#   ]

#   # ARNs of AWS entities who can assume these roles
#   #trusted_role_arns = [
#   #  "arn:aws:iam::307990089504:root",
#   #  "arn:aws:iam::835367859851:user/anton",
#   #]
  
#   # AWS Services that can assume these roles
#   trusted_role_services = [
#     "ops.apigateway.amazonaws.com",
#   ]
# }
############################
#
# module "lambda_replicator_role" {
#   source = "terraform-aws-modules/iam/aws//modules/iam-assumable-role"
#   role_name = "MpmLambdaReplicator"

#   create_role  = true
#   role_description = "role descriptions"

#   #List of ARNs of IAM policies to attach to IAM role
#   custom_role_policy_arns = [
#     "arn:aws:iam::aws:policy/aws-service-role/AWSLambdaReplicator",
#   ]

#   # ARNs of AWS entities who can assume these roles
#   #trusted_role_arns = [
#   #  "arn:aws:iam::307990089504:root",
#   #  "arn:aws:iam::835367859851:user/anton",
#   #]
  
#   # AWS Services that can assume these roles
#   trusted_role_services = [
#     "replicator.lambda.amazonaws.com",
#   ]
# }