# S3 bucket: mpm-front
module "s3-bucket-website-front" {
  source = "./modules/mpm-bucket"       
  bucket_name = "${var.app_name}-front"
  vpc_region = var.vpc_region
  is_website_bucket = true
  vpc_tags = var.vpc_tags
}

#  module "log_bucket" {
#  source  = "terraform-aws-modules/s3-bucket/aws"
#  version = "1.9.0"

#  bucket                         = "${local.bucket_name}-logs"  #-${random_pet.this.id}"
#  acl                            = "log-delivery-write"
#  force_destroy                  = true
#  attach_elb_log_delivery_policy = true
# }

# S3 bucket: mpm-front-login
module "s3-bucket-website-front-login" {
  source = "./modules/mpm-bucket"
  bucket_name = "${var.app_name}-front-login"
  vpc_region = var.vpc_region
  is_website_bucket = true
  vpc_tags = var.vpc_tags
}

module "s3-bucket-lambdas-ci-artifacts" {
  source = "./modules/mpm-bucket"
  bucket_name = "${var.app_name}-lambdas-ci-artifacts"
  vpc_region = var.vpc_region
  is_website_bucket = false
  vpc_tags = var.vpc_tags
}

# S3 bucket: mpm-inbound-data
module "s3-bucket-data-inbound" {
  source = "./modules/mpm-bucket"
  bucket_name = "${var.app_name}-inbound-data"
  vpc_region = var.vpc_region
  is_website_bucket = false
  vpc_tags = var.vpc_tags
}

# S3 bucket: mpm-inbound-data
module "s3-bucket-discourse-forum" {
  source = "./modules/mpm-bucket"
  bucket_name = "${var.app_name}-discourse-forum"
  vpc_region = var.vpc_region
  is_website_bucket = false
  vpc_tags = var.vpc_tags
}