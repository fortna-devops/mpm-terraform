#############################################################
# Data sources to get VPC and default security group details
#############################################################
data "aws_vpc" "insights_vpc" {
  id = module.vpc.vpc_id  
  # cidr_block = module.vpc.cidr 
  }

data "aws_security_group" "default" {
  name   = "default"
  vpc_id = data.aws_vpc.insights_vpc.id
}
#################
# Security groups
#################
resource "aws_security_group" "allow_tls" {
  name        = "allow_tls"
  description = "Allow TLS inbound traffic"
  vpc_id      = data.aws_vpc.insights_vpc.id

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]  
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = data.aws_vpc.insights_vpc.tags
 
}

module "http80_sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/http-80"
  version = "~> 3.0"

  name        = "http-ports-80-sg"   #"${module.vpc.vpc_id}-http-sg"
  description = "HTTP ports open for everybody"
  vpc_id      = data.aws_vpc.insights_vpc.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  #tags = data.aws_vpc.insights_vpc.tags
}

module "http443_sg" {
  source  = "terraform-aws-modules/security-group/aws//modules/https-443"
  version = "~> 3.0"

  name        = "http-port-443-sg"   #"${module.vpc.vpc_id}-http-sg"
  description = "HTTPS ports open for everybody"
  vpc_id      = data.aws_vpc.insights_vpc.id

  ingress_cidr_blocks = ["0.0.0.0/0"]
  #tags = data.aws_vpc.insights_vpc.tags

}

############################
# Example of security group
############################
# resource "aws_security_group" "app_servers" {
#   name_prefix = "${var.app_name}-prod-"     #"app-servers-"
#   description = "For application servers"
#   vpc_id      =  var.vpc_id  #data.aws_vpc.default.id
# }

# resource "aws_security_group_rule" "allow_access" {
#   type                     = "ingress"
#   from_port                = module.aurora.this_rds_cluster_port
#   to_port                  = module.aurora.this_rds_cluster_port
#   protocol                 = "tcp"
#   source_security_group_id = aws_security_group.app_servers.id
#   security_group_id        = module.aurora.this_security_group_id
# }