locals {
  db_master_user_name    = var.environment == "prod" || var.environment == "production" ? "mast5r_convey0r" : "postgres"
  db_deletion_protection = var.environment == "prod" || var.environment == "production" ? true : false
  db_publicly_accessible = var.environment == "prod" || var.environment == "production" ? true : false
  db_storage_encrypted   = var.environment == "prod" || var.environment == "production" ? true : false
}

#############
# RDS Aurora
#############
module "aurora" {
  source  = "terraform-aws-modules/rds-aurora/aws"
  version = "2.20.0"

  name                            = module.vpc.name
  engine                          = "aurora-postgresql"
  engine_version                  = "11.6"
  subnets                         = module.vpc.private_subnets 
  vpc_id                          = module.vpc.vpc_id  
  replica_count                   = 1
  instance_type                   = "db.r5.large"
  username                        = local.db_master_user_name
  publicly_accessible             = local.db_publicly_accessible
  # A List of ARNs for the IAM roles to associate to the RDS Cluster.
  # iam_roles = []

  #Specifies whether the underlying storage layer should be encrypted
  # Don't encrypt Dev bc we may want to export and restart schema somewhere else
  storage_encrypted               = local.db_storage_encrypted

  # List of log types to export to cloudwatch
  # enabled_cloudwatch_logs_exports = []

  # Specifies whether IAM Database authentication should be enabled or not. 
  # Not all versions and instances are supported. Refer to the AWS documentation to see which versions are supported
  # iam_database_authentication_enabled  = false

  apply_immediately               = true
  skip_final_snapshot             = true
  db_parameter_group_name         = aws_db_parameter_group.aurora_db_postgres11_parameter_group.id
  db_cluster_parameter_group_name = aws_rds_cluster_parameter_group.aurora_cluster_postgres11_parameter_group.id
  //  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]

  iam_database_authentication_enabled = true

  create_security_group = false

  #A list of Security Group ID's to allow access to.
  allowed_security_groups = [aws_security_group.aurora-ps-sg.id]
  security_group_description = ""

  #List of VPC security groups to associate to the cluster in addition to the SG we create in this module
  # vpc_security_group_ids = 

  # Name for an automatically created database on cluster creation
   database_name = "testdb"

  # DB snapshot to create this database from
  # snapshot_identifier = ""

  deletion_protection = local.db_deletion_protection
}

resource "aws_db_parameter_group" "aurora_db_postgres11_parameter_group" {
  name        = "${module.vpc.name}-aurora-db-postgres11-parameter-group"
  family      = "aurora-postgresql11"
  description = "${module.vpc.name}-aurora-db-postgres11-parameter-group"
}

resource "aws_rds_cluster_parameter_group" "aurora_cluster_postgres11_parameter_group" {
  name        = "${module.vpc.name}-aurora-postgres11-cluster-parameter-group"
  family      = "aurora-postgresql11"
  description = "${module.vpc.name}-aurora-postgres11-cluster-parameter-group"
}

############################
# Security group
############################
#SG for databases: Allow only VPC internal ip addresses
resource "aws_security_group" "aurora-ps-sg" {
  name        = "aurora-db-sg"
  description = "Access within VPC"
  vpc_id      = data.aws_vpc.insights_vpc.id

  ingress {
    description = "Connections from within VPC"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [module.vpc.vpc_cidr_block]   #only VPC addresses
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = data.aws_vpc.insights_vpc.tags
}

# resource "aws_security_group_rule" "allow_access" {
#   type                     = "ingress"
#   from_port                = module.aurora.this_rds_cluster_port
#   to_port                  = module.aurora.this_rds_cluster_port
#   protocol                 = "tcp"
#   source_security_group_id = aws_security_group.aurora-ps-sg.id
#   security_group_id        = module.aurora.this_security_group_id
# }
